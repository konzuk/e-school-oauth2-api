module.exports.push = function(playerID, obj) { // playerID [array]
    var title = obj.title || "You got one notify from 711Go";
    var data = {
        app_id: __cofig.onesignal.id,
        contents: { "en": title },
        small_icon: "http://" + __server + "/assets/img/ignore/conpany_logo.png",
        data: obj,
        content_available: true,
        ios_badgeType: 'Increase',
        ios_badgeCount: 1,
        badge_count: 1,
        badge: "Increment",
        include_player_ids: playerID
        // included_segments: ["mobile","mobile-android","All"]
    };

    var headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + __cofig.onesignal.key
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers,
        badge: 'Increment',
    };
    var https = require('https');
    try {
        var reqNTF = https.request(options, function(resNtf) {
            resNtf.on('data', function(data) {
                console.log("Response:");
                console.log(JSON.parse(data));
                return;
            });
        });
        reqNTF.on('error', function(e) {
            console.log("ERROR NOTIFICATION:");
            console.log(e);
            return;

        });
        reqNTF.write(JSON.stringify(data));
        reqNTF.end();
    } catch (err) {
        console.log("ERROR NOTIFICATION:");
        console.log(err);
        return;
    }
}

module.exports.send_sms = function(phone, sms) {
    var https = require('http');

    var data = `?username=` + __cofig.sms.user + `&pass=` + __cofig.sms.pass + `&sender=` + __cofig.sms.sender + `&isflash=0&smstext=` + __cofig.sms.default_text + `+` + sms + `&gsm=` + phone;
    var options = {
        host: "client.mekongsms.com",
        path: "/api/sendsms.aspx" + data,
        method: "GET"
    };

    https.request(options, function(response) {
        var str = '';
        //another chunk of data has been recieved, so append it to `str`
        response.on('data', function(chunk) {
            rs = '' + chunk;
            var n = rs.indexOf("]");
            var ss = rs.substring(0, n + 1);
            str += ss;
            console.log(str);
        });
        //the whole response has been recieved, so we just print it out here
        response.on('end', function() {
            console.log(str);
        });
    }).end();
}