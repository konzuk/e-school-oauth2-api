﻿var response = require(__base + 'app/config/response');
var db = require(__base + 'app/driver/mysql');
var auth = require(__base + 'app/config/authentication');


module.exports.login = function(req, res) {
    if (req.method === 'POST') {
        var username = req.body.username || "";
        var password = req.body.password || "";
        if(username == "" && password == ""){
            response.error(req, res, 406, "USERNAME_OR_PASSWORD_IS_REQUIRED");
        }
        else if (username == "" || password == "") {
            response.error(req, res, 406, "USERNAME_AND_PASSWORD_IS_REQUIRED");
        }
        else{
            var qSelect = [
                'std_id',
                'std_school_name',
                'std_first_name',
                'std_last_name',
                'std_gender_id',
                'std_dob',
				'pay_to_date'
            ];
            db.connect(function(cnn) {
                var sql = 'SELECT ?? FROM wb_student_info '+  
				'LEFT JOIN wb_payment ON wb_payment.pay_id = wb_student_info.std_payment_id '+
				'WHERE std_username = ' + cnn.escape(username) + ' AND std_password = ' + cnn.escape(password) + ' LIMIT 1';

                cnn.query(sql, [qSelect], function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length == 1) {
                        userData = rows[0];
                        
                        var token = auth.setToken(userData);
                        userData.token = token;
                        
                        response.json(req, res, {
                            'data': userData,
                            'status': true
                        });
                        return;
                    } else {
                        response.error(req,res,401,"USERNAME_AND_PASSWORD_INCORRECT");	
                    }
                });
            });
        }
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.facebook_login = function(req, res) {
    if (req.method === 'POST') {
        var facebookID = req.body.facebook_id  || "";
        if(facebookID == ""){
            response.error(req, res, 406, "FACEBOOK_ID_IS_REQUIRED");
        }else{
            var qSelect = [
                'std_id',
                'std_school_name',
                'std_first_name',
                'std_last_name',
                'std_gender_id',
                'std_dob',
				'pay_to_date'
            ];
            db.connect(function(cnn) {
                var sql = 'SELECT ?? FROM wb_student_info '+  
				'LEFT JOIN wb_payment ON wb_payment.pay_id = wb_student_info.std_payment_id '+
				'WHERE std_fb_id = ' + facebookID + ' LIMIT 1';

                cnn.query(sql, [qSelect], function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length == 1) {
                        userData = rows[0];
                        
                        var token = auth.setToken(userData);
                        userData.token = token;
                        
                        response.json(req, res, {
                            'data': userData,
                            'status': true
                        });
                        return;
                    } else {
                        response.error(req,res,401,"FACEBOOK_ID_IS_INCORRECT");	
                    }
                });
            });
        }
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};

module.exports.login_facebook = function(req, res) {
    if (req.method === 'POST') {
        var facebook_id = req.body.facebook_id || "";
        var first_name = req.body.first_name || "";
        var last_name = req.body.last_name || "";
        if(facebook_id == ""){
            response.error(req, res, 406, "FACEBOOK_ID_IS_REQUIRED");
        }
        else{
            var qSelect = [
                'std_id',
                'std_school_name',
                'std_first_name',
                'std_last_name',
                'std_gender_id',
                'std_dob',
                'pay_to_date'
            ];
            db.connect(function(cnn) {
                var sql = 'SELECT ?? FROM wb_student_info LEFT JOIN wb_payment ON wb_payment.pay_id = wb_student_info.std_payment_id WHERE std_fb_id = ' + cnn.escape(facebook_id) + ' LIMIT 1';
                cnn.query(sql, [qSelect], function(err, rows, fields) {
                    if (err) throw err;
                    if (rows.length == 1) {
                        userData = rows[0];
                        
                        var token = auth.setToken(userData);
                        userData.token = token;
                        
                        response.json(req, res, {
                            'data': userData,
                            'status': true
                        });
                        return;
                    } else {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var yyyy = today.getFullYear();
                        today = yyyy+'-'+mm+'-'+dd;
                        console.log(today);
                        var sql = `INSERT INTO wb_student_info (std_first_name, std_last_name, std_fb_id, std_user_activated, std_register_date)
                                    VALUES (`+ cnn.escape(first_name) +`, `+ cnn.escape(last_name) +`, `+ cnn.escape(facebook_id) +`, 1, `+cnn.escape(today)+`)`;
                        cnn.query(sql, function(err, rows, fields) {
                            if (err) throw err;
                            if(!err){                                
                                response.json(req, res, {
                                    'status': true
                                });
                                return;

                            }                            
                        });
                    }
                });
            });
        }
    } else {
        response.error(req, res, 405, "METHOD_DISALLOW");
    }
};