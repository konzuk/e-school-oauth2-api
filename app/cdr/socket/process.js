var db = require(__base + 'app/driver/mysql'),
    notification = require('../notification/push'),
    _app = {
        book_again: 0,
        book_distance: 5,
    };

module.exports.dispatching = function(id, next) {    
    bookdetail.field = [
        "bok_id",
        "driver_id",
        "bok_pickup_lat",
        "bok_pickup_lng",
        "bok_drop_lat",
        "bok_drop_lng",
        "bok_complete_trip",
        "bok_date",
        "bok_drop_name",
        "bok_pickup_name",
        "bok_request_by",
        "bok_status",
        "bok_type",
        "client_id",
        "booking_request.created_by",
        "req_driver_id"
    ];
    data = {};
    book_detail(bookdetail, function(bd) {
        var returnData = bd;
        returnData.bok_date = __formatDatetime(returnData.bok_date);
        driverData = {
            bok_id: id
        }
        driverData.field = [
            "dvr_id",
            "dvr_fullname",
            "dvr_country_iso2",
            "dvr_country_code",
            "dvr_phone",
            "dvr_lat",
            "dvr_lng",
            "car_plate_number",
            "car_year",
            "car_color",
            "car_mode",
            "dvr_photo"
        ];
        book_detail(driverData, function(driver) {
            returnData.driver = driver
            returnData.driver.host = __server_host + 'driver/';
            one = 1;
            trip = {
                slat: bd.bok_pickup_lat,
                slng: bd.bok_pickup_lng,
                elat: bd.bok_drop_lat,
                elng: bd.bok_drop_lng
            }
            arrive = {
                slat: driver.dvr_lat,
                slng: driver.dvr_lng,
                elat: bd.bok_pickup_lat,
                elng: bd.bok_pickup_lng
            }
            distances(arrive, function(ok, d) {
                returnData.arrive = {};
                returnData.trip = {};
                returnData.arrive.distanceValue = 0;
                returnData.arrive.durationValue = 0;
                if (ok) {
                    returnData.arrive.distanceValue = d.distanceValue;
                    returnData.arrive.durationValue = d.durationValue;
                }
                if (trip.elat != 0) {
                    distances(trip, function(ok, rs) {
                        returnData.trip.distanceValue = 0;
                        returnData.trip.durationValue = 0;
                        if (ok) {
                            returnData.trip.distanceValue = rs.distanceValue;
                            returnData.trip.durationValue = rs.durationValue;
                        }
                        if (one === 1) {
                            one = 2;
                            next(returnData, null);
                        }
                    });
                } else {
                    returnData.trip.distanceValue = 0;
                    returnData.trip.durationValue = 0;
                    if (one === 1) {
                        one = 2;
                        next(returnData, null);
                    }
                }
            });
        })
    });
}